# README

## 前提条件

・SFDXプロジェクトが設定されていること
・「.vscode」-「settings.json」に、以下設定値を追加

  "salesforcedx-vscode-core.retrieve-test-code-coverage": true

## 起動条件

１：.sfdx > tools > testresults > apex
　　test-result-codecoverage.json　が存在していること
（SFDXのApexテストを行うと、ファイルが生成されます）

２：テストを行った該当のApexをエディタ上に表示します

上記条件が揃うと、エディタ上にカバーしていない行を装飾します。

## 操作

下部に表示されているステータスバーに
テストカバー値が表示されます。
クリックすると、行の装飾を切替できます。