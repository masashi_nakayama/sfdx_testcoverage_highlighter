// VSCodeのモジュールをインポート
import * as vscode from 'vscode';

let myStatusBarItem: vscode.StatusBarItem;

var view = true;
var uncoveredLineStyle: vscode.TextEditorDecorationType;


function uncoveredLineStyleClear(): void {

    uncoveredLineStyle = vscode.window.createTextEditorDecorationType({

        backgroundColor: 'rgba(90,30,15,1)',
        overviewRulerColor: 'rgba(90,30,15,1)',
        overviewRulerLane: vscode.OverviewRulerLane.Right,
        isWholeLine: true,
        light: {
            // this color will be used in light color themes
            borderColor: 'rgba(90,30,15,1)'
        },
        dark: {
            // this color will be used in dark color themes
            borderColor: 'rgba(90,30,15,1)'
        },
    });

}

// 拡張機能を作成するときには、「activate」関数をexportする必要があります
// ロード時に一度だけ呼ばれます
export function activate({ subscriptions }: vscode.ExtensionContext) {

    console.log('SFDX Test Coverage Highlighter Activate');
    vscode.window.showInformationMessage('SFDX Test Coverage Highlighter Activate');

    myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
    myStatusBarItem.command = 'extension.sfdxTestCoverageSwitch';

    subscriptions.push(myStatusBarItem);
    subscriptions.push(vscode.window.onDidChangeActiveTextEditor(uncoverTestHighlight));

    uncoveredLineStyleClear();
    uncoverTestHighlight();

    // vscodeモジュールのcommands.registerCommandの呼び出し
    // commandは、「extension.sayHellow」で、実行時には、
    // 「（） => 」引数なしで呼び出され、「vscode.window.showInformationMessage('Hello World!');」が実行
    // let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
        // create a new status bar item that we can now manage
        // 情報タイプのメッセージを表示
        // コマンドが実行されるたびに、extension.helloWorld が呼ばれて、この関数のみ実行される
        // vscode.window.showInformationMessage('Hello World!!! ');
    // 拡張機能が使用されなくなったときに、リソース開放を行うための、設定。
    // 今回は、registerCommandの指定をを開放する必要があるので、開放対象に追加する
	// context.subscriptions.push(disposable);

}


let disposable = vscode.commands.registerCommand('extension.sfdxTestCoverageSwitch', () => {

    view = view ? false : true;

    uncoveredLineStyle.dispose();

    uncoveredLineStyleClear();
    uncoverTestHighlight();

});

function uncoverTestHighlight(): void {

    myStatusBarItem.hide();

    // Declare variables
    let fs = require("fs");
    let folderName = vscode.workspace.name; // get the open folder name
    let folderPath = vscode.workspace.rootPath; // get the open folder path
    // console.log('WORKSPACE:' + folderName);
    // console.log('ROOTPATH:' + folderPath);

    let testFiles = JSON.parse(fs.readFileSync(folderPath + '/.sfdx/tools/testresults/apex/test-result-codecoverage.json', { encoding: "utf-8" }));
    // console.log('fs.readFileSync:' + testFiles);

    const path = require("path");
    // const coverFilePath = path.resolve(folderPath, "./.sfdx/tools/testresults/apex/test-result-codecoverage.json");
    // let testFiles = require(coverFilePath);
    // console.log(testFiles);

    let activeEditor = vscode.window.activeTextEditor;
    var uncoveredLineOptions: vscode.DecorationOptions[] = [];

    if (activeEditor) {

        // console.log(activeEditor);
        var activeEditorFileName = activeEditor.document.fileName;
        // console.log(activeEditorFileName);
        var activeFileName = path.basename(activeEditorFileName).replace(/\.[^/.]+$/, "");
        // console.log(activeFileName);

        for (var testFile in testFiles) {

            var r = testFiles[testFile];

            if ( r['name'] == activeFileName ) {
                
                if ( view ) {

                    // console.log(r['name']);
                    
                    for (var line in r['lines']) {

                        var lineNo = parseInt(line) - 1;
                        var lineVal = r['lines'][line];
                        // console.log(lineNo + ':' + lineVal);

                        if (lineVal == 0) {
                            let decorationRange: vscode.DecorationOptions = { range: activeEditor.document.lineAt(lineNo).range };
                            uncoveredLineOptions.push(decorationRange);
                        }
                    }
                }

                // console.log(r['coveredPercent']);
                if ( r['coveredPercent'] ) {

                    var eyeIcon;
                    if (view) {
                        eyeIcon = '$(eye)';
                    } else {
                        eyeIcon = '$(eye-closed)';
                    }

                    var coveredIcon;
                    if ( r['coveredPercent'] >= 75 ) {
                        coveredIcon = '$(check)';
                    } else {
                        coveredIcon = '$(alert)';
                    }

                    myStatusBarItem.text = coveredIcon + ' ' + r['coveredPercent'] + '% Test Coverd：' + eyeIcon;
                    myStatusBarItem.show();
                }

            }
        }

        activeEditor.setDecorations(uncoveredLineStyle, uncoveredLineOptions);

    }

    vscode.commands.executeCommand('workbench.action.files.revert');

}


// 拡張機能が非アクティブ化されたときに、実行されます。
export function deactivate() {}
